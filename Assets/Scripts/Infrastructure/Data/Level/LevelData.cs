﻿using Infrastructure.Data.EnemyWave;
using Infrastructure.Data.Zone;
using UnityEngine;

namespace Infrastructure.Data.Level
{
    [CreateAssetMenu(fileName = "Level Data", menuName = "Design / Level Data")]
    public class LevelData : ScriptableObject
    {
        public string Id;
        public int Reward;
        public ZoneType ZoneType;
        public WaveType WaveType;
    }
}