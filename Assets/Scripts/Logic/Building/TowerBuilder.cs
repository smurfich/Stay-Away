﻿using Framework.Code.Infrastructure.Services.PersistentProgress;
using Infrastructure.Data.Tower;
using Infrastructure.Services.Factories.TowerBuilding;
using Logic.TowerBehaviour;
using Logic.UpgradeSystem.TowerUpgradeSystem;
using UnityEngine;
using Zenject;

namespace Logic.Building
{
    public class TowerBuilder : MonoBehaviour
    {
        const int TowerBuildingCost = 100;

        [SerializeField] TowerBuilderView towerBuilderView;
        [SerializeField] Transform spawnPoint;

        TowerData towerData;
        float towerRadius;

        IPersistentProgressService progressService;
        ITowerFactory towerFactory;

        Tower towerInstance;
        TowerUpgradePresenter towerUpgradeInstance;


        [Inject]
        void Construct(IPersistentProgressService progressService, ITowerFactory towerFactory)
        {
            this.progressService = progressService;
            this.towerFactory = towerFactory;
        }

        public void Construct(TowerData towerData, float towerRadius)
        {
            this.towerData = towerData;
            this.towerRadius = towerRadius;
        }

        public void Enable()
        {
            towerFactory.Load();

            progressService.Progress.Collectables.FireAmountChanged += OnFireAmountChanged;
        }

        public void Disable()
        {
            progressService.Progress.Collectables.FireAmountChanged -= OnFireAmountChanged;
            towerBuilderView.Disable();

            if (towerInstance != null)
                towerInstance.Disable();

            if (towerUpgradeInstance != null)
                towerUpgradeInstance.Disable();
        }

        void OnFireAmountChanged()
        {
            if (progressService.Progress.Collectables.FireCoinsAmount < TowerBuildingCost) return;

            towerBuilderView.Enable();

            towerBuilderView.BuyButtonClicked += OnBuyButtonClicked;

            progressService.Progress.Collectables.FireAmountChanged -= OnFireAmountChanged;
        }

        void OnBuyButtonClicked()
        {
            CreateTower();
            CreateTowerUpgrade(towerInstance);

            progressService.Progress.Collectables.TakeAwayFireCoins(TowerBuildingCost);

            towerBuilderView.Disable();
            towerBuilderView.BuyButtonClicked -= OnBuyButtonClicked;
        }

        void CreateTower()
        {
            towerInstance = towerFactory.CreateTower(spawnPoint.position - new Vector3(0f,1f,0f), transform.parent);
            towerInstance.Construct(towerData.FrequencyValue, towerData.DamageValue);
            towerInstance.Enable();
        }

        void CreateTowerUpgrade(Tower tower)
        {
            towerUpgradeInstance = towerFactory.CreateTowerUpgrade();
            towerUpgradeInstance.transform.SetParent(transform.parent);
            towerUpgradeInstance.Construct(tower);
            towerUpgradeInstance.Enable();
        }
    }
}