﻿using System.Collections.Generic;
using System.Linq;
using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using Infrastructure.Services;
using Infrastructure.Services.GoogleSheets;

namespace Infrastructure.Data.EnemyWave
{
    public class EnemyWaveDataProcessor : DataProcessor
    {
        const int Time = 0;
        const int Delay = 1;
        const int EnemyId = 2;
        const int Amount = 3;
        const int SpawnDelay = 4;

        public override bool NeedRefresh => false;

        protected override string[] AllTableId { get; } =
        {
            CsvData.CvsId[5],
            CsvData.CvsId[6],
            CsvData.CvsId[7],
            CsvData.CvsId[8],
            CsvData.CvsId[9],
        };

        readonly Dictionary<string, WaveType> waveTypes = new Dictionary<string, WaveType>
        {
            {CsvData.CvsId[5], WaveType.Tutor_01},
            {CsvData.CvsId[6], WaveType.Easy_02},
            {CsvData.CvsId[7], WaveType.Level_03},
            {CsvData.CvsId[8], WaveType.Level_04},
            {CsvData.CvsId[9], WaveType.Level_05}
        };

        EnemyWaveData[] enemyWaveData;
        readonly List<EnemyWaveData> tempWaveData = new List<EnemyWaveData>();

        public EnemyWaveDataProcessor(CsvLoader csvLoader, IAssetProvider assetProvider, ISheetParser sheetParser) :
            base(csvLoader, assetProvider, sheetParser)
        {
        }


        public override void Init()
        {
            enemyWaveData = AssetProvider.LoadCollection<EnemyWaveData>(AssetPath.EnemyWave);
            tempWaveData.AddRange(enemyWaveData);

            base.Init();
        }

        protected override void OnDataLoaded(string result, string gid)
        {
            if (tempWaveData.Count == 0) return;

            EnemyWaveData waveData = tempWaveData.FirstOrDefault();
            if (waveData == null) return;

            string[] rows = SheetParser.SplitTableRows(result);

            InitializeArrays(rows, waveData);

            for (int i = 1; i < rows.Length; i++)
            {
                string[] cells = rows[i].Split(Separator);

                float time = SheetParser.ParseFloat(cells[Time]);
                float delay = SheetParser.ParseFloat(cells[Delay]);
                string enemyId = cells[EnemyId];
                int amount = SheetParser.ParseInt(cells[Amount]);
                float spawnDelay = SheetParser.ParseFloat(cells[SpawnDelay]);

                waveData.WaveType = waveTypes[gid];
                waveData.Time[i - 1] = time;
                waveData.Delay[i - 1] = delay;
                waveData.EnemyId[i - 1] = enemyId;
                waveData.Amount[i - 1] = amount;
                waveData.SpawnDelay[i - 1] = spawnDelay;
            }

            tempWaveData.Remove(waveData);
        }

        void InitializeArrays(IReadOnlyCollection<string> rows, EnemyWaveData waveData)
        {
            waveData.Time = new float[rows.Count - 1];
            waveData.Delay = new float[rows.Count - 1];
            waveData.EnemyId = new string[rows.Count - 1];
            waveData.Amount = new int[rows.Count - 1];
            waveData.SpawnDelay = new float[rows.Count - 1];
        }

        public EnemyWaveData Load(WaveType waveType) =>
            enemyWaveData.FirstOrDefault(waveData => waveData.WaveType == waveType);
    }
}