﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Framework.Code.Infrastructure.Signals;
using Framework.Code.Infrastructure.States;
using Infrastructure.ObjectPool.Enemies;
using Infrastructure.Services.GoogleSheets;
using Logic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Enemies.Spawning
{
	public class EnemySpawner : MonoBehaviour, ILevelEnder
	{
		[SerializeField] Transform center;
		[SerializeField] Transform parentCanvas;
		[SerializeField] float radius;

		Coroutine spawnCoroutine;
		IEnemyPool enemyPool;

		readonly List<GameObject> enemies = new List<GameObject>();

		[Inject]
		void Construct(IEnemyPool enemyPool)
		{
			this.enemyPool = enemyPool;
		}

		public void EndLevel()
		{
			if (spawnCoroutine != null)
				StopCoroutine(spawnCoroutine);

			foreach (GameObject enemy in enemies.Where(e => e.activeInHierarchy))
			{
				enemy.GetComponent<EnemyMovement>().Stop();
				enemy.GetComponent<EnemyHealth>().Die(DamageDealerType.Touch);
			}
		}

		public void SpawnEnemy(EnemyType enemyType, int amount, float delay)
		{
			if (spawnCoroutine != null)
				StopCoroutine(spawnCoroutine);

			spawnCoroutine = StartCoroutine(SpawnEnemyRoutine(enemyType, amount, delay));
		}

		IEnumerator SpawnEnemyRoutine(EnemyType enemyType, int amount, float delay)
		{
			for (int i = 0; i < amount; i++)
			{
				GameObject enemy = enemyPool.Get(enemyType, transform, center, parentCanvas);
				enemy.SetActive(true);
				enemy.GetComponent<EnemyHealth>().ResetParameters();
				enemy.transform.position = RandomCircle();

				enemies.Add(enemy);

				yield return new WaitForSeconds(delay);
			}
		}

		Vector3 RandomCircle()
		{
			// Vector3 randomPos = Random.insideUnitSphere * radius;
			// randomPos += transform.position;
			// randomPos.y = 0.5f;
			//          
			// Vector3 direction = randomPos - transform.position;
			// direction.Normalize();
			//          
			// float dotProduct = Vector3.Dot(transform.forward, direction);
			// dotProduct = Random.Range(0.6f, 0.6f);
			// float dotProductAngle = Mathf.Acos(dotProduct / transform.forward.magnitude * direction.magnitude);
			//
			// randomPos.x = Mathf.Cos(dotProductAngle) * radius + transform.position.x;
			// randomPos.z = Mathf.Sin(dotProductAngle * (Random.value > 0.5f ? 1f : -1f)) * radius + transform.position.z;
			//
			// return randomPos;

			float ang = Random.value * 360;
			//ang = Mathf.Clamp(ang, 210, 290);
			Vector3 pos;
			pos.x = center.position.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
			pos.y = 0.5f;
			pos.z = center.position.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
			return pos;
		}

		void OnDestroy()
		{
			enemyPool.Clear();
		}
	}
}