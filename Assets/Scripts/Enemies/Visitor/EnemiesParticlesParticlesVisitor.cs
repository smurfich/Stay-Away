﻿using Control;
using Infrastructure.ObjectPool.Particles;
using Logic;
using PlayerZone;
using UnityEngine;

namespace Enemies.Visitor
{
	public class EnemiesParticlesParticlesVisitor : IEnemiesParticlesVisitor
	{
		readonly IParticlePool particlePool;
		readonly EnemyHealth enemyHealth;

		ParticleSystem damageFromBulletParticle;
		ParticleSystem deathFromBulletParticle;
		ParticleSystem damageFromTapParticle;
		ParticleSystem deathFromTapParticle;
		ParticleSystem deathFromZoneParticle;

		public EnemiesParticlesParticlesVisitor(IParticlePool particlePool, EnemyHealth enemyHealth)
		{
			this.particlePool = particlePool;
			this.enemyHealth = enemyHealth;
		}

		public void Visit(Bullet bullet)
		{
			LoadDamageFromBulletParticle();
			PlayDamageFromBulletParticle();

			if (enemyHealth.IsDead)
			{
				LoadDeathFromBulletParticle();
				PlayDeathFromBulletParticle();
			}
		}

		public void Visit(TouchDetector touchDetector)
		{
			LoadDamageFromTapParticle();
			PlayDamageFromTapParticle();

			if (enemyHealth.IsDead)
			{
				LoadDeathFromTapParticle();
				PlayDeathFromTapParticle();
			}
		}

		public void Visit(Zone zone)
		{
			if (enemyHealth.IsDead)
			{
				LoadDeathFromZoneParticle();
				PlayDeathFromZoneParticle();
			}
		}

		void LoadDamageFromBulletParticle()
		{
			if (damageFromBulletParticle == null)
				damageFromBulletParticle = particlePool.GetBulletDamageParticle();
		}

		void LoadDeathFromBulletParticle()
		{
			if (deathFromBulletParticle == null)
				deathFromBulletParticle = particlePool.GetEnemyDeathParticle(DamageDealerType.Bullet);
		}

		void LoadDamageFromTapParticle()
		{
			if (damageFromTapParticle == null)
				damageFromTapParticle = particlePool.GetTapDamageParticle();
		}

		void LoadDeathFromTapParticle()
		{
			if (deathFromTapParticle == null)
				deathFromTapParticle = particlePool.GetEnemyDeathParticle(DamageDealerType.Touch);
		}

		void LoadDeathFromZoneParticle()
		{
			if (deathFromZoneParticle == null)
				deathFromZoneParticle = particlePool.GetEnemyDeathParticle(DamageDealerType.Zone);
		}

		void PlayDamageFromBulletParticle()
		{
			damageFromBulletParticle.transform.position = enemyHealth.transform.position;
			damageFromBulletParticle.Play();
		}

		void PlayDeathFromBulletParticle()
		{
			deathFromBulletParticle.transform.position = enemyHealth.transform.position;
			deathFromBulletParticle.transform.localScale = enemyHealth.transform.localScale * 1.75f;
			deathFromBulletParticle.Play();
		}

		void PlayDamageFromTapParticle()
		{
			damageFromTapParticle.transform.position = enemyHealth.transform.position;
			damageFromTapParticle.Play();
		}

		void PlayDeathFromTapParticle()
		{
			deathFromTapParticle.transform.position = enemyHealth.transform.position;
			deathFromTapParticle.transform.localScale = enemyHealth.transform.localScale * 1.75f;
			deathFromTapParticle.Play();
		}

		void PlayDeathFromZoneParticle()
		{
			deathFromZoneParticle.transform.position = enemyHealth.transform.position;
			deathFromZoneParticle.transform.localScale = enemyHealth.transform.localScale * 1.75f;
			deathFromZoneParticle.Play();
		}
	}
}