﻿namespace Infrastructure.Data.Zone
{
    public enum ZoneType
    {
        Simple, 
        Fast, 
        Standard
    }
}