﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Infrastructure.ObjectPool.Bullets;
using Infrastructure.ObjectPool.Particles;
using UnityEngine;
using Zenject;

namespace Logic.TowerBehaviour
{
	public class Gun : MonoBehaviour
	{
		const string EnemyLayerName = "Enemy";

		[SerializeField] Transform bulletSpawnPoint;
		[SerializeField] float attackRadius;

		IBulletsPool bulletsPool;
		IParticlePool particlePool;

		ParticleSystem shootParticle;

		float currentDamage;
		float rotationTime;

		Tween rotationTween;

		[Inject]
		void Construct(IBulletsPool bulletsPool, IParticlePool particlePool)
		{
			this.bulletsPool = bulletsPool;
			this.particlePool = particlePool;
		}

		public void Construct(float currentDamage, float rotationTime)
		{
			this.currentDamage = currentDamage;
			this.rotationTime = rotationTime;
		}


		public void Enable()
		{
			LoadShootParticle();
			bulletsPool.Load();
		}

		public void Disable()
		{
			bulletsPool?.Clear();
		}

		public void RefreshDamage(float currentDamage) => this.currentDamage = currentDamage;

		public void RefreshRotationTime(float rotationTime) => this.rotationTime = rotationTime;

		public void Shoot()
		{
			List<Collider> enemies = DetectEnemies();
			if (enemies == null) return;

			Collider closestEnemy = FindClosestEnemy(enemies);
			if (closestEnemy == null) return;

			Quaternion lookRotation = GetLookRotationToEnemy(closestEnemy.transform);
			Vector3 direction = GetDirectionToEnemy(closestEnemy.transform);

			rotationTween?.Kill();
			rotationTween = transform.DORotateQuaternion(lookRotation, rotationTime).OnComplete(() =>
			{
				SpawnBullet(direction);
				PlayShootParticle();
			});
		}

		void SpawnBullet(Vector3 direction)
		{
			Bullet bullet = bulletsPool.Get();
			bullet.gameObject.SetActive(true);
			bullet.transform.SetParent(transform.root);

			bullet.transform.position = bulletSpawnPoint.position;
			bullet.ResetVelocity();
			bullet.ResetCollider();
			bullet.Construct(currentDamage);
			bullet.Fire(direction);
		}

		void PlayShootParticle()
		{
			shootParticle.transform.position = bulletSpawnPoint.position;
			shootParticle.Play();
		}

		void LoadShootParticle()
		{
			if (shootParticle == null)
				shootParticle = particlePool.GetTowerShootParticle();
		}

		Vector3 GetDirectionToEnemy(Transform closestEnemy)
		{
			Vector3 closestEnemyPosition = closestEnemy.position;
			Vector3 position = transform.TransformPoint(bulletSpawnPoint.localPosition);

			closestEnemyPosition.y = position.y;

			return (closestEnemyPosition - position).normalized;
		}

		Quaternion GetLookRotationToEnemy(Transform closestEnemy)
		{
			Vector3 closestEnemyPosition = closestEnemy.position;
			Vector3 position = transform.position;

			closestEnemyPosition.y = position.y;

			Vector3 direction = (closestEnemyPosition - position).normalized;

			return Quaternion.LookRotation(direction);
		}

		List<Collider> DetectEnemies() => Physics
			.OverlapSphere(transform.position, attackRadius, LayerMask.GetMask(EnemyLayerName)).ToList();

		Collider FindClosestEnemy(List<Collider> enemiesColliders)
		{
			if (enemiesColliders == null) return null;

			Collider closestCollider = enemiesColliders.FirstOrDefault();

			float distance = Mathf.Infinity;
			Vector3 position = transform.position;

			foreach (Collider enemyCollider in enemiesColliders)
			{
				Vector3 difference = enemyCollider.transform.position - position;
				float currentDistance = difference.sqrMagnitude;

				if (currentDistance < distance)
				{
					distance = currentDistance;
					closestCollider = enemyCollider;
				}
			}

			return closestCollider;
		}
	}
}