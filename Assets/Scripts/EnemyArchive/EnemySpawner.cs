﻿using System.Collections;
using Framework.Code.Infrastructure.States;
using UnityEngine;
using Zenject;

namespace EnemyArchive
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] Transform target;
        [SerializeField] float spawnInterval = 1f;

        EnemyPoolManager enemyPoolManager;
        GameStateMachine gameStateMachine;
        bool isAbleToSpawn;
    
        [Inject]
        void Construct(EnemyPoolManager enemyPoolManager, GameStateMachine gameStateMachine)
        {
            this.enemyPoolManager = enemyPoolManager;
            this.gameStateMachine = gameStateMachine;
        }
    
        // void OnEnable()
        // {
        //     GameLoopState.onEnterGameLoopState += OnEnterGameLoop;
        // }
        //
        // void OnDisable()
        // {
        //     GameLoopState.onEnterGameLoopState -= OnEnterGameLoop;
        // }

        void OnEnterGameLoop()
        {
            isAbleToSpawn = true;

            StartCoroutine(SpawnEnemyWithInterval());
        }

        IEnumerator SpawnEnemyWithInterval()
        {
            while (isAbleToSpawn)
            {
                var enemy = enemyPoolManager.GetEnemyFromPool();
                if (!enemy) continue;
                enemy.transform.position = transform.position;
                enemy.GetComponent<Enemy>().SetTarget(target);
                yield return new WaitForSeconds(spawnInterval);
            }
        }

        void Update()
        {
            if (gameStateMachine.ActiveState is GameLoopState == false) isAbleToSpawn = false;
        }
    }
}
