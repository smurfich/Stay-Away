﻿using System;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Infrastructure;
using Infrastructure.Data.Tower;
using Logic.TowerBehaviour;

namespace Logic.UpgradeSystem.TowerUpgradeSystem
{
    public class TowerUpgradeModel
    {
        public event Action<int> FrequencyUpgraded;
        public event Action<int> DamageUpgraded;

        readonly TowerDataProcessor towerDataProcessor;
        readonly Tower tower;
        readonly IPersistentProgressService progressService;

        float currentFrequencyValue;
        float currentDamageValue;

        int frequencyUpgradeLevel;
        int damageUpgradeLevel;

        public int CurrentFrequencyCost { get; private set; }

        public int CurrentDamageCost { get; private set; }


        public TowerUpgradeModel(TowerDataProcessor towerDataProcessor, Tower tower,
            IPersistentProgressService progressService)
        {
            this.towerDataProcessor = towerDataProcessor;
            this.tower = tower;
            this.progressService = progressService;
        }

        public void BuyFrequencyUpgrade()
        {
            if (CanBuyFrequencyUpgrade() == false) return;

            progressService.Progress.Collectables.TakeAwayFireCoins(CurrentFrequencyCost);

            tower.RefreshFrequency(currentFrequencyValue);

            frequencyUpgradeLevel++;
            FrequencyUpgraded?.Invoke(frequencyUpgradeLevel);
        }

        public void BuyDamageUpgrade()
        {
            if (CanBuyDamageUpgrade() == false) return;

            progressService.Progress.Collectables.TakeAwayFireCoins(CurrentDamageCost);

            tower.RefreshDamage(currentDamageValue);

            damageUpgradeLevel++;
            DamageUpgraded?.Invoke(damageUpgradeLevel);
        }

        public bool CanBuyFrequencyUpgrade()
        {
            UpgradeData frequencyUpgradeData = towerDataProcessor.GetFrequencyUpgradeData(frequencyUpgradeLevel + 1);
            if (frequencyUpgradeData.Value == 0) return false;

            currentFrequencyValue = frequencyUpgradeData.Value;
            CurrentFrequencyCost = frequencyUpgradeData.UpgradeCost;

            return progressService.Progress.Collectables.FireCoinsAmount >= frequencyUpgradeData.UpgradeCost;
        }

        public bool CanBuyDamageUpgrade()
        {
            UpgradeData damageUpgradeData = towerDataProcessor.GetDamageUpgradeData(damageUpgradeLevel + 1);
            if (damageUpgradeData.Value == 0) return false;

            currentDamageValue = damageUpgradeData.Value;
            CurrentDamageCost = damageUpgradeData.UpgradeCost;

            return progressService.Progress.Collectables.FireCoinsAmount >= damageUpgradeData.UpgradeCost;
        }

        public bool HasFrequencyUpgrades()
        {
            UpgradeData frequencyUpgradeData = towerDataProcessor.GetFrequencyUpgradeData(frequencyUpgradeLevel + 1);
            return frequencyUpgradeData.Value > 0;
        }

        public bool HasDamageUpgrades()
        {
            UpgradeData damageUpgradeData = towerDataProcessor.GetDamageUpgradeData(damageUpgradeLevel + 1);
            return damageUpgradeData.Value > 0;
        }
    }
}