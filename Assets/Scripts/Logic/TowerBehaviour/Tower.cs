﻿using Infrastructure;
using UnityEngine;

namespace Logic.TowerBehaviour
{
	public class Tower : MonoBehaviour
	{
		float rotationTime;
		float startRotationTime;
		float frequencyValue;
		float damageValue;

		float currentFrequency;
		float currentDamage;

		bool isEnabled;
		float nextShootTime;

		Gun gun;
		TowerAnimations towerAnimations;

		public void Construct(float frequencyValue, float damageValue)
		{
			this.frequencyValue = frequencyValue;
			this.damageValue = damageValue;
		}

		public void Enable()
		{
			isEnabled = true;
			SetupStats();
			SetupGun();
			SetupAnimations();
		}

		void SetupStats()
		{
			currentFrequency = frequencyValue;
			currentDamage = damageValue;
			rotationTime = currentFrequency / Config.RotationTimeDivider;
			startRotationTime = rotationTime;
		}

		void SetupGun()
		{
			gun = GetComponentInChildren<Gun>();
			gun.Construct(currentDamage, rotationTime);
			gun.Enable();
		}

		void SetupAnimations()
		{
			towerAnimations = GetComponent<TowerAnimations>();
			towerAnimations.Enable();
		}

		public void RefreshFrequency(float frequency)
		{
			Config.RotationTimeDivider = currentFrequency / startRotationTime;

			currentFrequency = frequency;
			rotationTime = currentFrequency / Config.RotationTimeDivider;
			gun.RefreshRotationTime(rotationTime);

			towerAnimations.PlayKnobScaleAnimation();
			towerAnimations.PlaySpeedUpParticle();
		}

		public void RefreshDamage(float damage)
		{
			currentDamage = damage;
			gun.RefreshDamage(currentDamage);

			towerAnimations.PlayDropScaleAnimation();
			towerAnimations.PlayDamageUpParticle();
		}

		public void Disable()
		{
			isEnabled = false;
			gun.Disable();
		}

		void Update()
		{
			if (isEnabled == false) return;

			if (!(Time.time > nextShootTime)) return;

			Fire();

			nextShootTime = Time.time + 1 / currentFrequency;
		}

		void Fire()
		{
			gun.Shoot();
		}
	}
}