﻿using System;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;

namespace Infrastructure.Data.Enemies
{
	[CreateAssetMenu(fileName = "Enemy Data", menuName = "Design / Enemy Data")]
	public class EnemyData : ScriptableObject
	{
		public EnemyType EnemyType;
		public float StartHealth;
		public float StartSpeed;
		public float StartDamage;
		public float StartScale;
		public int Reward;
		public GameObject EnemyPrefab;

		public bool IsLoaded;
	}
}