﻿using System;
using System.Collections.Generic;
using System.Linq;
using Enemies;
using Infrastructure.Services.Factories.Enemies;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;

namespace Infrastructure.ObjectPool.Enemies
{
	public class EnemyPool : IEnemyPool
	{
		public event Action<GameObject> EnemyDied;

		readonly IEnemyFactory enemyFactory;
		readonly List<GameObject> enemies = new List<GameObject>();

		public EnemyPool(IEnemyFactory enemyFactory)
		{
			this.enemyFactory = enemyFactory;
		}

		public GameObject Get(EnemyType enemyType, Transform parent, Transform target, Transform parentCanvas)
		{
			if (enemies.Count != 0)
			{
				GameObject enemy = enemies.FirstOrDefault(e => e.GetComponent<EnemyHealth>().EnemyType == enemyType);
				if (enemy != null)
				{
					enemies.Remove(enemy);
					return enemy;
				}
			}

			GameObject newEnemy = AddObject(enemyType, parent, target, parentCanvas);
			enemies.Remove(newEnemy);

			return newEnemy;
		}

		public void Clear() => enemies.Clear();

		void ReturnToPool(GameObject objectToReturn)
		{
			objectToReturn.gameObject.SetActive(false);
			enemies.Add(objectToReturn);
			EnemyDied?.Invoke(objectToReturn);
		}

		GameObject AddObject(EnemyType enemyType, Transform parent, Transform target, Transform parentCanvas)
		{
			GameObject newObject = enemyFactory.CreateEnemy(enemyType, parent, target, parentCanvas);
			newObject.GetComponent<EnemyHealth>().Death += ReturnToPool;
			newObject.gameObject.SetActive(false);

			enemies.Add(newObject);

			return newObject;
		}
	}
}