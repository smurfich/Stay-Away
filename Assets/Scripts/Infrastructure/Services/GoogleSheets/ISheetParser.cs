﻿using UnityEngine;

namespace Infrastructure.Services.GoogleSheets
{
	public interface ISheetParser
	{
		Vector3 ParseVector3(string s);
		int ParseInt(string s);
		float ParseFloat(string s);
		string[] SplitTableRows(string rawData);
		float ParseFloatWithComma(string s);
	}
}