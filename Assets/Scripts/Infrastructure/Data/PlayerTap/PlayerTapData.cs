﻿using UnityEngine;

namespace Infrastructure.Data.PlayerTap
{
    [CreateAssetMenu(fileName = "Player Tap Data", menuName = "Design / Player Tap", order = 0)]
    public class PlayerTapData : ScriptableObject
    {
        public float AreaValue;
        public int AreaCost;
        public float DamageValue;
        public int DamageCost;
    }
}