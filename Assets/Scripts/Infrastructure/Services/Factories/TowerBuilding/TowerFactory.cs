﻿using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using Logic;
using Logic.TowerBehaviour;
using Logic.UpgradeSystem.TowerUpgradeSystem;
using UnityEngine;
using Zenject;

namespace Infrastructure.Services.Factories.TowerBuilding
{
    public class TowerFactory : ITowerFactory
    {
        readonly DiContainer container;
        readonly IAssetProvider assetProvider;


        Tower towerPrefab;
        TowerUpgradePresenter towerUpgradePrefab;

        public TowerFactory(DiContainer container, IAssetProvider assetProvider)
        {
            this.container = container;
            this.assetProvider = assetProvider;
        }


        public void Load()
        {
            towerPrefab = assetProvider.Load<Tower>(AssetPath.Tower);
            towerUpgradePrefab = assetProvider.Load<TowerUpgradePresenter>(AssetPath.TowerUpgrade);
        }

        public Tower CreateTower(Vector3 spawnPoint, Transform parent) =>
            container.InstantiatePrefabForComponent<Tower>(towerPrefab, spawnPoint, Quaternion.identity, parent);

        public TowerUpgradePresenter CreateTowerUpgrade() =>
            container.InstantiatePrefabForComponent<TowerUpgradePresenter>(towerUpgradePrefab);
    }
}