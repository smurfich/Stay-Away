﻿using System.Collections.Generic;

namespace Infrastructure.Services
{
    public static class CsvData
    {
        public static readonly string[] CvsId =
        {
            "",
            "&gid=498965198",
            "&gid=391672533",
            "&gid=235579043",
            "&gid=309361528",
            "&gid=544202914",
            "&gid=1986094143",
            "&gid=550072026",
            "&gid=1513212610",
            "&gid=1751358132"
        };

        public static readonly Dictionary<string, string> CsvNames = new Dictionary<string, string>
        {
            {CvsId[0], "enemies"},
            {CvsId[1], "tap"},
            {CvsId[2], "tower"},
            {CvsId[3], "levels"},
            {CvsId[4], "zones"},
            {CvsId[5], "wave_tutor_01"},
            {CvsId[6], "wave_easy_02"},
            {CvsId[7], "wave_level_03"},
            {CvsId[8], "wave_level_04"},
            {CvsId[9], "wave_level_05"},
        };
    }
}