﻿using System;
using Infrastructure;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;

namespace Enemies
{
	public class EnemyHealth : MonoBehaviour
	{
		const float MinimumScale = 0.5f;

		public event Action<float> HealthChanged;
		public event Action<GameObject> Death;
		public event Action<DamageDealerType> DamageTaken;
		public event Action<DamageDealerType> FullHealthDamageTaken;
		public bool IsDead { get; private set; }
		public EnemyType EnemyType { get; private set; }

		public float StartHealth { get; private set; }

		bool IsAlive => currentHealth > 0;

		float startScale;

		float currentHealth;
		float currentScale;

		float scaleCoefficient;

		public void Construct(float health, float scale, EnemyType enemyType)
		{
			StartHealth = health;
			startScale = scale;

			currentScale = startScale;
			currentHealth = StartHealth;

			EnemyType = enemyType;

			CalculateScaleCoefficient();
			UpdateScale();
		}

		public void ResetParameters()
		{
			currentScale = startScale;
			currentHealth = StartHealth;
			IsDead = false;
			UpdateScale();
		}

		void CalculateScaleCoefficient() => scaleCoefficient = currentScale / currentHealth;

		void UpdateScale() => transform.SetScale(currentScale);

		public void TakeDamage(float damage, DamageDealerType damageDealerType)
		{
			currentHealth -= damage;
			currentScale = currentHealth * scaleCoefficient;
			currentScale = Mathf.Clamp(currentScale, MinimumScale, 20f);

			if (IsAlive == false)
			{
				if (damageDealerType == DamageDealerType.Bullet)
					DamageTaken?.Invoke(damageDealerType);
				
				Die(damageDealerType);
			}
			else
			{
				DamageTaken?.Invoke(damageDealerType);
				HealthChanged?.Invoke(currentHealth);
				UpdateScale();
			}
		}

		public void Die(DamageDealerType damageDealerType)
		{
			IsDead = true;

			FullHealthDamageTaken?.Invoke(damageDealerType);
			Death?.Invoke(gameObject);
		}
	}

	public enum DamageDealerType
	{
		Touch,
		Bullet,
		Zone
	}
}