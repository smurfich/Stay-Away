﻿using System.Collections;

namespace Logic
{
	public interface ILevelEnder
	{
		void EndLevel();
	}
}