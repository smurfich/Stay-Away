﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    UnitStateMachine unitStateMachine;

    void Start()
    {
        //Initialize state machine
        unitStateMachine = new UnitStateMachine();
        
        //Create the states of player
        //var example = new ExampleState(sParam1);
        
        //Create transitions for states
        //AddTransition(stateFrom, stateTo, transitionTrigger);
        //unitStateMachine.AddAnyTransition(stateTo, transitionTrigger);
    }
    
    void AddTransition(IState from, IState to, Func<bool> condition) =>
        unitStateMachine.AddTransition(from, to, condition);
    
    //Transition triggers

    void Update()
    {
        unitStateMachine.Tick();
    }

    void FixedUpdate()
    {
        unitStateMachine.PhysicsTick();
    }
}
