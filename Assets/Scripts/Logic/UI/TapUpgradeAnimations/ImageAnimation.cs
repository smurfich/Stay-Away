﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Logic.UI.TapUpgradeAnimations
{
	[Serializable]
	public class ImageAnimation
	{
		public Image ArrowImage;
		public float EndPoint;
		[NonSerialized] public Vector3 StartPosition;
		[NonSerialized] public Tween MoveTween;
		[NonSerialized] public Tween FadeTween;
	}
}