﻿using System;

namespace Framework.Code.Data
{
	[Serializable]
	public class PlayerProgress
	{
		public int Level;
		public int WaveRepeatingCounter;
		public CollectablesData Collectables;
		public TapUpgrade TapUpgrade;
		public EnemyStatsCoefficient EnemyStatsCoefficient;

		public PlayerProgress()
		{
			Level = 1;
			Collectables = new CollectablesData();
			TapUpgrade = new TapUpgrade();
			EnemyStatsCoefficient = new EnemyStatsCoefficient();
		}
	}

	[Serializable]
	public class EnemyStatsCoefficient
	{
		const float IncreaseDamageCoefficient = 1.15f;
		const float IncreaseHealthCoefficient = 1.19f;
		const float IncreaseRewardCoefficient = 1.3f;

		public float DamageCoefficient;
		public float HealthCoefficient;
		public float RewardCoefficient;

		public EnemyStatsCoefficient()
		{
			DamageCoefficient = 1f;
			HealthCoefficient = 1f;
			RewardCoefficient = 1f;
		}

		public void MultiplyDamageCoefficient()
		{
			DamageCoefficient *= IncreaseDamageCoefficient;
		}

		public void MultiplyHealthCoefficient()
		{
			HealthCoefficient *= IncreaseHealthCoefficient;
		}

		public void MultiplyRewardCoefficient()
		{
			RewardCoefficient *= IncreaseRewardCoefficient;
		}
	}
}