﻿using System.Threading.Tasks;
using Enemies;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Infrastructure.Data.Enemies;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;
using Zenject;

namespace Infrastructure.Services.Factories.Enemies
{
	public class EnemyFactory : IEnemyFactory
	{
		readonly EnemiesDataProcessor enemiesDataProcessor;
		readonly DiContainer diContainer;
		IPersistentProgressService progressService;

		public EnemyFactory(EnemiesDataProcessor enemiesDataProcessor, DiContainer diContainer,
			IPersistentProgressService progressService)
		{
			this.enemiesDataProcessor = enemiesDataProcessor;
			this.diContainer = diContainer;
			this.progressService = progressService;
		}

		public async Task<GameObject> CreateEnemyAsync(EnemyType enemyType, Transform parent, Transform target,Transform parentCanvas)
		{
			EnemyData enemyData = await enemiesDataProcessor.LoadAsync(enemyType);

			//GameObject enemyInstance = diContainer.InstantiatePrefabForComponent<GameObject>(enemyData.EnemyPrefab, parent);

			GameObject enemyInstance = Object.Instantiate(enemyData.EnemyPrefab, parent);

			enemyInstance.GetComponent<EnemyAttack>().Construct(enemyData.StartDamage *
			                                                    progressService.Progress.EnemyStatsCoefficient
				                                                    .DamageCoefficient);
			enemyInstance.GetComponent<EnemyMovement>().Construct(enemyData.StartSpeed, target);
			enemyInstance.GetComponent<EnemyHealth>()
				.Construct(enemyData.StartHealth * progressService.Progress.EnemyStatsCoefficient.HealthCoefficient,
					enemyData.StartScale, enemyData.EnemyType);
			enemyInstance.GetComponent<EnemyReward>().Construct(enemyData.Reward, parentCanvas);

			return enemyInstance;
		}

		public GameObject CreateEnemy(EnemyType enemyType, Transform parent, Transform target, Transform parentCanvas)
		{
			EnemyData enemyData = enemiesDataProcessor.Load(enemyType);

			GameObject enemyInstance = diContainer.InstantiatePrefab(enemyData.EnemyPrefab, parent);

			enemyInstance.GetComponent<EnemyAttack>().Construct(enemyData.StartDamage *
			                                                    progressService.Progress.EnemyStatsCoefficient
				                                                    .DamageCoefficient);
			enemyInstance.GetComponent<EnemyMovement>().Construct(enemyData.StartSpeed, target);
			enemyInstance.GetComponent<EnemyHealth>()
				.Construct(enemyData.StartHealth * progressService.Progress.EnemyStatsCoefficient.HealthCoefficient,
					enemyData.StartScale, enemyData.EnemyType);
			enemyInstance.GetComponent<EnemyReward>().Construct(Mathf.RoundToInt(enemyData.Reward *
				progressService.Progress.EnemyStatsCoefficient.RewardCoefficient), parentCanvas);

			enemyInstance.GetComponent<EnemyAttack>().Enable();


			return enemyInstance;
		}
	}
}