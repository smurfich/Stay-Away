﻿using Framework.Code;
using Framework.Code.Factories.Levels;
using Framework.Code.Infrastructure.Services.Assets;
using Infrastructure.Data.EnemyWave;
using Infrastructure.Data.Zone;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;

namespace Infrastructure.Data.Level
{
    public class LevelDataProcessor : DataProcessor
    {
        const int Id = 0;
        const int Reward = 1;
        const int Zone = 2;
        const int Wave = 3;

        public override bool NeedRefresh => true;
        protected override string[] AllTableId => new[] {"&gid=235579043"};

        LevelData levelData;

        readonly ILevelFactory levelFactory;

        bool isParsed;
        bool isLoaded;

        string rawData;


        public LevelDataProcessor(CsvLoader csvLoader, IAssetProvider assetProvider, ISheetParser sheetParser,
            ILevelFactory levelFactory) : base(
            csvLoader, assetProvider, sheetParser)
        {
            this.levelFactory = levelFactory;
        }


        public override void Init()
        {
            levelData = AssetProvider.Load<LevelData>(AssetPath.LevelData);
            base.Init();
        }


        protected override void OnDataLoaded(string result, string gid)
        {
            rawData = result;
            ParseData(result);
        }

        void ParseData(string result)
        {
            string[] rows = SheetParser.SplitTableRows(result);

            for (int i = 1; i < rows.Length; i++)
            {
                string[] cells = rows[i].Split(Separator);

                string id = cells[Id];

                if (id == levelFactory.CurrentLevel.Id)
                {
                    string zone = cells[Zone].Trim();
                    string wave = cells[Wave].Trim();
                    
                    int reward = SheetParser.ParseInt(cells[Reward]);
                    ZoneType zoneType = zone.GetZoneType();
                    WaveType waveType = wave.GetWaveType();

                    levelData.Id = id;
                    levelData.Reward = reward;
                    levelData.ZoneType = zoneType;
                    levelData.WaveType = waveType;
                }
            }

            isParsed = true;
            isLoaded = true;
        }


        public LevelData Load() => levelData;

        // public async Task<LevelData> Load()
        // {
        //     while (!isLoaded)
        //     {
        //         await Task.Yield();
        //     }
        //
        //     return levelData;
        // }
    }
}