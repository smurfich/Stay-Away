﻿using System.Collections;
using Framework.Code.Infrastructure.Signals;
using Framework.Code.Infrastructure.States;
using Infrastructure;
using Infrastructure.Data.EnemyWave;
using Logic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Enemies.Spawning
{
	public class EnemyWave : MonoBehaviour, ILevelEnder
	{
		[SerializeField] EnemySpawner[] enemySpawners;

		EnemyWaveData waveData;
		SignalBus signalBus;
		Coroutine waveCoroutine;

		[Inject]
		void Construct(SignalBus signalBus)
		{
			this.signalBus = signalBus;
		}

		public void Construct(EnemyWaveData waveData) => this.waveData = waveData;

		public void Enable()
		{
			waveCoroutine = StartCoroutine(StartWave());

			signalBus.Subscribe<StateChangedSignal>(OnStateChanged);
		}

		public void Disable()
		{
			signalBus.TryUnsubscribe<StateChangedSignal>(OnStateChanged);

			if (waveCoroutine != null)
				StopCoroutine(waveCoroutine);
		}

		void OnStateChanged(StateChangedSignal signal)
		{
			if (signal.State is WinState || signal.State is LoseState)
			{
				if (waveCoroutine != null)
					StopCoroutine(waveCoroutine);
			}
		}

		IEnumerator StartWave()
		{
			for (int i = 0; i < waveData.Time.Length; i++)
			{
				yield return new WaitForSeconds(waveData.Delay[i]);

				EnemySpawner randomSpawner = enemySpawners[Random.Range(0, enemySpawners.Length)];
				randomSpawner.SpawnEnemy(waveData.EnemyId[i].GetEnemyType(), waveData.Amount[i],
					waveData.SpawnDelay[i]);
			}

			StartCoroutine(StartWave());
		}

		public void EndLevel()
		{
			if (waveCoroutine != null)
				StopCoroutine(waveCoroutine);
		}
	}
}