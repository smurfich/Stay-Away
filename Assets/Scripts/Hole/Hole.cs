﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using EnemyArchive;
using Framework.Code.Infrastructure.States;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

public class Hole : MonoBehaviour
{
    [SerializeField] float loseValue = 0.25f;
    [SerializeField] float winValue = 7f;
    [SerializeField] float initialSize = 2f;
    [SerializeField] float timeToGrow = 3f;
    [SerializeField] float growValue = 1f;
    [SerializeField] float growSpeed = 0.5f;
    [SerializeField] float reducePerSecond = 1f;
    [SerializeField] int maxHoleHp = 3;
    [SerializeField] GameObject mainCircle;
    [SerializeField] GameObject targetCircle;
    
    bool isCanGrowth;
    Tween changingSizeTween;
    float targetScale = Mathf.Infinity;
    Tween increaseSizeTween;
    int currentHoleHP;
    GameStateMachine gameStateMachine;
    
    [Inject]
    void Construct(GameStateMachine stateMachine)
    {
        this.gameStateMachine = stateMachine;
    }

    void OnEnable()
    {
        //GameLoopState.onEnterGameLoopState += OnEnterGameLoop;
    }

    void Start()
    {
        ResizeHole(initialSize, true);
        currentHoleHP = maxHoleHp;
        targetCircle.transform.localScale = new Vector3(winValue, winValue, winValue);
    }

    void OnEnterGameLoop()
    {
        isCanGrowth = true;
        
        IncreaseSizeAfterDelay();
    }

    void ResizeHole(float amount, bool isInstant = false)
    {
        var newValue = Mathf.Max(0, mainCircle.transform.localScale.x + amount);
        
        if (isInstant)
        {
            mainCircle.transform.localScale = new Vector3(newValue, newValue, newValue);
        }
        else
        {
            changingSizeTween?.Kill(false);
            changingSizeTween = mainCircle.transform.DOScale(new Vector3(newValue, newValue, newValue), growSpeed);
        }
    }

    void FixedUpdate()
    {
        if (gameStateMachine.ActiveState is GameLoopState == false) return;
        CheckHoleSize();
        CheckProgress();
    }

    void CheckProgress()
    {
        if (gameStateMachine.ActiveState is GameLoopState == false) return;
        if (mainCircle.transform.localScale.x < loseValue)
        {
            gameStateMachine.Enter<LoseState>();
        }

        if (mainCircle.transform.localScale.x > winValue)
        {
            gameStateMachine.Enter<WinState>();
        }
    }

    void CheckHoleSize()
    {
        if (mainCircle.transform.localScale.x > targetScale)
        {
            isCanGrowth = false;
            increaseSizeTween?.Kill(false);
            changingSizeTween?.Kill(false);
            ResizeHole(Time.fixedDeltaTime * -reducePerSecond, true);
            
            MMVibrationManager.Haptic(HapticTypes.RigidImpact);
        }
        else {
            isCanGrowth = true;
            IncreaseSizeAfterDelay();
        }
    }

    void IncreaseSizeAfterDelay()
    {
        if (!isCanGrowth || increaseSizeTween.IsActive()) return;
        targetScale = mainCircle.transform.localScale.x + growValue;
        increaseSizeTween?.Kill(false);
        increaseSizeTween = DOVirtual.DelayedCall(timeToGrow, () =>
        {
            ResizeHole(growValue);
            IncreaseSizeAfterDelay();
        });
    }

    void OnTriggerEnter(Collider other)
    {
        var enemy = other.GetComponent<Enemy>();
        if (enemy)
        {
            if (!enemy.IsAlive) return;
            currentHoleHP -= 1;

            if (currentHoleHP == 0)
            {
                currentHoleHP = maxHoleHp;
                
                targetScale = mainCircle.transform.localScale.x - enemy.DamageToSize;
            }
            
            enemy.HandleHoleEntrance();
        }
    }

    void OnDisable()
    {
        changingSizeTween?.Kill();
        increaseSizeTween?.Kill();
        
       //GameLoopState.onEnterGameLoopState -= OnEnterGameLoop;
    }

    void OnDestroy()
    {
        changingSizeTween?.Kill();
        increaseSizeTween?.Kill();
    }
}
