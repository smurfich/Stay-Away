﻿using UnityEngine;

namespace Infrastructure.Data.EnemyWave
{
    [CreateAssetMenu(fileName = "Enemy Wave", menuName = "Design / Enemy Wave")]
    public class EnemyWaveData : ScriptableObject
    {
        public WaveType WaveType;
        public float[] Time;
        public float[] Delay;
        public string[] EnemyId;
        public int[] Amount;
        public float[] SpawnDelay;
    }
}