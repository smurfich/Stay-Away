﻿using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Infrastructure;
using Logic;
using ProceduralPrimitivesUtil;
using UnityEngine;

namespace PlayerZone
{
	public class ZoneOutline : MonoBehaviour, ILevelEnder
	{
		[SerializeField] ParticleSystem outlineParticle;
		Tween scaleTween;

		public bool IsStopped => outlineParticle.isStopped;
		
		public void SetScale(float scaleFactor) => transform.SetScale(scaleFactor);

		public void UpdateScale(float endValue, float speed)
		{
			float distance = Mathf.Abs(endValue - transform.localScale.x);
			float duration = distance / speed;

			scaleTween?.Kill();
			scaleTween = transform.DOScale(endValue, duration);
		}

		public void Stop() => outlineParticle.Stop();

		public void Play() => outlineParticle.Play();
		
		public void EndLevel()
		{
			outlineParticle.Stop();
		}
	}
}