﻿using System;
using System.Collections;
using DG.Tweening;
using Enemies;
using Enemies.Visitor;
using Framework.Code.Infrastructure.States;
using Infrastructure;
using Infrastructure.Data.Zone;
using Logic;
using ProceduralPrimitivesUtil;
using UnityEngine;
using Zenject;

namespace PlayerZone
{
	public class Zone : DamageDealer, ILevelEnder
	{
		public event Action<float> SizeChanged;

		[SerializeField] GameObject mainCircle;
		[SerializeField] Tube sandCircle;
		[SerializeField] TriggerObserver triggerObserver;
		[SerializeField] ZoneOutline zoneOutline;
		[SerializeField] ParticleSystem winParticle;
		[SerializeField] float tweenSpeed = 2f;
		[SerializeField] float outlineScaleOffset = 0f;

		float startRadius;
		float endRadius;
		float growthSpeed;
		float strength;
		float maxHealth;

		float currentRadius;
		ZoneData zoneData;

		float currentHealth;
		float scaleRate;
		float outlineScaleRate;

		Tween downScaleTween;
		Tween upScaleTween;

		GameStateMachine stateMachine;

		[Inject]
		void Construct(GameStateMachine stateMachine)
		{
			this.stateMachine = stateMachine;
		}

		public void Construct(ZoneData zoneData)
		{
			this.zoneData = zoneData;
		}

		public void Enable()
		{
			UpdateSettings();
			SetStartSize();

			triggerObserver.TriggerEntered += OnTriggerEntered;
		}

		public void Disable()
		{
			downScaleTween?.Kill();
			upScaleTween?.Kill();
		}

		public void DisableInnerCircle() => sandCircle.GetComponent<MeshRenderer>().enabled = false;

		void FixedUpdate()
		{
			if ((stateMachine.ActiveState is GameLoopState) == false) return;

			if (downScaleTween.IsActive()) return;

			IncreaseSize();
		}

		void UpdateSettings()
		{
			startRadius = zoneData.StartRadius;
			endRadius = zoneData.EndRadius;
			growthSpeed = zoneData.GrowthSpeed;
			strength = zoneData.Strength;
			maxHealth = zoneData.Health;

			currentHealth = maxHealth * startRadius;

			startRadius *= Config.ScaleCoefficient;
			endRadius *= Config.ScaleCoefficient;

			currentRadius = startRadius;

			scaleRate = startRadius / currentHealth;

			// don't use startRadius in coefficient calculation! final outlineScaleRate should be the same for all levels!
			var radiusCoefficient = 0.25f * Config.ScaleCoefficient;
			outlineScaleRate = (radiusCoefficient / Config.PrimitivesScaleDivider - outlineScaleOffset) /
				radiusCoefficient * 0.95f;
		}

		void OnTriggerEntered(Collider other)
		{
			var enemyAttack = other.GetComponent<EnemyAttack>();
			if (enemyAttack == null) return;

			TakeDamage(enemyAttack);
			UpdateScale(ref downScaleTween, tweenSpeed);

			enemyAttack.GetComponent<EnemyHealth>().Die(DamageDealerType.Zone);

			triggerObserver.UpdateScale(currentRadius, tweenSpeed);

			float outlineRadius = currentRadius * outlineScaleRate;
			zoneOutline.UpdateScale(outlineRadius, tweenSpeed);
			zoneOutline.Stop();
		}


		void IncreaseSize()
		{
			currentHealth += growthSpeed * Time.fixedDeltaTime;

			currentRadius = currentHealth * scaleRate;
			UpdateScale(ref upScaleTween, growthSpeed);

			triggerObserver.UpdateScale(currentRadius, growthSpeed);

			if (zoneOutline.IsStopped)
				zoneOutline.Play();

			float outlineRadius = currentRadius * outlineScaleRate;
			zoneOutline.UpdateScale(outlineRadius, growthSpeed);
		}

		void SetStartSize()
		{
			mainCircle.transform.SetScale(endRadius);

			mainCircle.transform.localScale =
				new Vector3(mainCircle.transform.localScale.x, 1f, mainCircle.transform.localScale.z);

			sandCircle.radius1 = endRadius / Config.PrimitivesScaleDivider;
			sandCircle.radius2 = currentRadius / Config.PrimitivesScaleDivider;
			sandCircle.Apply();

			zoneOutline.SetScale(currentRadius / Config.PrimitivesScaleDivider - outlineScaleOffset);
			triggerObserver.SetScale(startRadius);
		}

		void TakeDamage(EnemyAttack enemyAttack)
		{
			float damage = enemyAttack.Damage / strength;

			currentHealth -= damage;
			currentRadius = currentHealth * scaleRate;
		}

		void UpdateScale(ref Tween tween, float speed = 1f)
		{
			float distance = Mathf.Abs(sandCircle.radius2 - currentRadius / Config.PrimitivesScaleDivider);
			float duration = distance / speed;

			if (tween != null)
			{
				SizeChanged?.Invoke(currentRadius);
				tween.Kill();
			}

			tween = DOVirtual
				.Float(sandCircle.radius2, currentRadius / Config.PrimitivesScaleDivider, duration, value =>
				{
					sandCircle.radius2 = value;
					sandCircle.Apply();
				})
				.OnComplete(() => SizeChanged?.Invoke(currentRadius));
		}

		public void EndLevel()
		{
			zoneOutline.gameObject.SetActive(false);
			winParticle.Play();
		}

		public override void Accept(IEnemiesParticlesVisitor particlesVisitor) => particlesVisitor.Visit(this);
	}
}