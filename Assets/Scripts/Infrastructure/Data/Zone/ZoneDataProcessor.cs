﻿using System.Collections.Generic;
using System.Linq;
using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using Infrastructure.Services.GoogleSheets;

namespace Infrastructure.Data.Zone
{
    public class ZoneDataProcessor : DataProcessor
    {
        const int Id = 0;
        const int StartRadius = 1;
        const int EndRadius = 2;
        const int GrowthSpeed = 3;
        const int Strength = 4;
        const int Health = 5;
        const int DeathRadius = 6;

        public override bool NeedRefresh => false;
        protected override string[] AllTableId => new[] {"&gid=309361528"};

        ZoneData[] zoneData;

        public ZoneDataProcessor(CsvLoader csvLoader, IAssetProvider assetProvider, ISheetParser sheetParser) : base(
            csvLoader, assetProvider, sheetParser)
        {
        }


        public override void Init()
        {
            zoneData = AssetProvider.LoadCollection<ZoneData>(AssetPath.ZoneData);
            base.Init();
        }

        protected override void OnDataLoaded(string result, string gid)
        {
            string[] rows = SheetParser.SplitTableRows(result);

            List<ZoneData> tempZoneData = new List<ZoneData>();
            tempZoneData.AddRange(zoneData);

            for (int i = 1; i < rows.Length; i++)
            {
                string[] cells = rows[i].Split(Separator);

                string id = cells[Id];
                float startRadius = SheetParser.ParseFloat(cells[StartRadius]);
                float endRadius = SheetParser.ParseFloat(cells[EndRadius]);
                float growthSpeed = SheetParser.ParseFloat(cells[GrowthSpeed]);
                float strength = SheetParser.ParseFloat(cells[Strength]);
                float health = SheetParser.ParseFloat(cells[Health]);
                float deathRadius = SheetParser.ParseFloat(cells[DeathRadius]);

                ZoneData first = tempZoneData.Last();

                first.ZoneType = id.GetZoneType();
                first.StartRadius = startRadius;
                first.EndRadius = endRadius;
                first.GrowthSpeed = growthSpeed;
                first.Strength = strength;
                first.Health = health;
                first.DeathRadius = deathRadius;

                tempZoneData.Remove(first);
            }
        }

        public ZoneData Load(ZoneType zoneType) => zoneData.First(zone => zone.ZoneType == zoneType);
    }
}