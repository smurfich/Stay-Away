﻿using Framework.Code.Factories.Levels;
using Infrastructure;

namespace Framework.Code.Infrastructure.States
{
    public class GameLoopState : IPayloadedState<Level>
    {
        Level currentLevel;

        public void Enter(Level level)
        {
            currentLevel = level;
        }

        public void Exit()
        {
            currentLevel.GetComponent<LevelBootstraper>().DisableEntities();
        }
    }
}