﻿using Infrastructure.ObjectPool.Particles;
using UnityEngine;
using Zenject;

namespace Enemies
{
	public class EnemyEffects : MonoBehaviour
	{
		[SerializeField] EnemyHealth enemyHealth;

		IParticlePool particlePool;

		[Inject]
		void Construct(IParticlePool particlePool)
		{
			this.particlePool = particlePool;
		}

		void Start()
		{
			enemyHealth.DamageTaken += OnDamageTaken;
			enemyHealth.FullHealthDamageTaken += OnFullHealthDamageTaken;
		}

		void OnDamageTaken(DamageDealerType damageDealerType)
		{
			ParticleSystem particle = null;

			switch (damageDealerType)
			{
				case DamageDealerType.Touch:
					particle = particlePool.GetTapDamageParticle();
					break;
				case DamageDealerType.Bullet:
					particle = particlePool.GetBulletDamageParticle();
					break;
			}

			if (particle == null) return;

			particle.transform.position = transform.position;
			particle.Play();
		}

		void OnFullHealthDamageTaken(DamageDealerType damageDealerType)
		{
			ParticleSystem particle = particlePool.GetEnemyDeathParticle(damageDealerType);
			particle.transform.position = transform.position;
			particle.transform.localScale = transform.localScale * 1.75f;
			                                //(damageDealer == DamageDealer.Bullet ? Vector3.one : Vector3.zero);
			particle.Play();
		}

		void OnDestroy()
		{
			enemyHealth.DamageTaken -= OnDamageTaken;
			enemyHealth.FullHealthDamageTaken -= OnFullHealthDamageTaken;
		}
	}
}