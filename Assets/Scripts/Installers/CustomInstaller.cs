using EnemyArchive;
using Infrastructure.Data.Enemies;
using Infrastructure.Data.EnemyWave;
using Infrastructure.Data.Level;
using Infrastructure.Data.PlayerTap;
using Infrastructure.Data.Tower;
using Infrastructure.Data.Zone;
using Infrastructure.ObjectPool.Bullets;
using Infrastructure.ObjectPool.Enemies;
using Infrastructure.ObjectPool.Particles;
using Infrastructure.Services.Factories.Bullets;
using Infrastructure.Services.Factories.Enemies;
using Infrastructure.Services.Factories.Particles;
using Infrastructure.Services.Factories.TowerBuilding;
using Infrastructure.Services.GoogleSheets;
using Logic.UI;
using Logic.UpgradeSystem.TapUpgradeSystem;
using UnityEngine;
using Zenject;

namespace Installers
{
	public class CustomInstaller : MonoInstaller
	{
		[SerializeField] EnemyPoolManager enemyPoolManager;
		[SerializeField] CsvLoader csvLoader;
		[SerializeField] TotalCoinsPassiveView totalCoinsPassiveView;

		public override void InstallBindings()
		{
			BindPoolManager();
			BindCsvLoader();
			BindSheetParser();
			BindEnemyFactory();
			BindProcessors();
			BindTotalCoinsPassiveView();
			BindEnemyPool();
			BindBulletsFactory();
			BindBulletsPool();
			BindTowerFactory();
			BindParticleFactory();
			BindParticlePool();
		}

		void BindProcessors()
		{
			// var enemiesDataProcessor = Container.Instantiate<EnemiesDataProcessor>();
			// var enemyWaveDataProcessor = Container.Instantiate<EnemyWaveDataProcessor>();
			// var levelDataProcessor = Container.Instantiate<LevelDataProcessor>();
			// var playerTapDataProcessor = Container.Instantiate<PlayerTapDataProcessor>();
			// var towerDataProcessor = Container.Instantiate<TowerDataProcessor>();
			// var zoneDataProcessor = Container.Instantiate<ZoneDataProcessor>();
			//
			// DataProcessor[] dataProcessors =
			// {
			//     enemiesDataProcessor,
			//     enemyWaveDataProcessor,
			//     levelDataProcessor,
			//     playerTapDataProcessor,
			//     towerDataProcessor,
			//     zoneDataProcessor
			// };

			Container.Bind<EnemiesDataProcessor>().FromNew().AsSingle();
			Container.Bind<EnemyWaveDataProcessor>().FromNew().AsSingle();
			Container.Bind<LevelDataProcessor>().FromNew().AsSingle();
			Container.Bind<ZoneDataProcessor>().FromNew().AsSingle();
			Container.Bind<PlayerTapDataProcessor>().FromNew().AsSingle();
			Container.Bind<TowerDataProcessor>().FromNew().AsSingle();
		}


		void BindSheetParser() => Container.Bind<ISheetParser>().To<SheetParser>().AsSingle();
		void BindCsvLoader() => Container.Bind<CsvLoader>().FromInstance(csvLoader).AsSingle();

		void BindPoolManager() => Container.Bind<EnemyPoolManager>().FromInstance(enemyPoolManager).AsSingle();

		void BindEnemyFactory() => Container.Bind<IEnemyFactory>().To<EnemyFactory>().AsSingle();

		void BindTotalCoinsPassiveView() =>
			Container.Bind<TotalCoinsPassiveView>().FromInstance(totalCoinsPassiveView).AsSingle();


		void BindEnemyPool() => Container.Bind<IEnemyPool>().To<EnemyPool>().AsSingle();
		void BindBulletsPool() => Container.Bind<IBulletsPool>().To<BulletsPool>().AsSingle();
		void BindBulletsFactory() => Container.Bind<IBulletsFactory>().To<BulletsFactory>().AsSingle();
		void BindTowerFactory() => Container.Bind<ITowerFactory>().To<TowerFactory>().AsSingle();
		void BindParticleFactory() => Container.Bind<IParticleFactory>().To<ParticleFactory>().AsSingle();
		void BindParticlePool() => Container.Bind<IParticlePool>().To<ParticlePool>().AsSingle();
	}
}