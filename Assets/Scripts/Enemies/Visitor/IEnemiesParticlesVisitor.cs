﻿using Control;
using Logic;
using PlayerZone;

namespace Enemies.Visitor
{
	public interface IEnemiesParticlesVisitor
	{
		void Visit(Bullet bullet);
		void Visit(TouchDetector touchDetector);
		void Visit(Zone zone);
	}
}