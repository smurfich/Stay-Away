﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnerFireScale : MonoBehaviour
{
    [Range(0,1)] [SerializeField] float arangeCoef = 0.25f;
    [SerializeField] float speed = 1f;

    Vector3 initialScale;

    void Start()
    {
        initialScale = transform.localScale;
    }

    void Update()
    {
        transform.localScale = initialScale * Mathf.Lerp(1 - arangeCoef, 1, Mathf.Abs(Mathf.Sin(Time.time * speed)));
    }
}
