﻿using DG.Tweening;
using Infrastructure.ObjectPool.Particles;
using UnityEngine;
using Zenject;

namespace Logic.TowerBehaviour
{
	public class TowerAnimations : MonoBehaviour
	{
		[SerializeField] Transform knob;
		[SerializeField] Transform drop;
		[SerializeField] float scaleFactor = 1.5f;
		[SerializeField] float scaleTweenDuration = 0.2f;

		IParticlePool particlePool;

		ParticleSystem speedUpParticle;
		ParticleSystem damageUpParticle;

		Tween knobScaleTween;
		Tween dropScaleTween;

		float startDropScale;
		float startKnobScale;

		[Inject]
		void Construct(IParticlePool particlePool)
		{
			this.particlePool = particlePool;
		}

		public void Enable()
		{
			startDropScale = drop.localScale.x;
			startKnobScale = knob.localScale.x;
			
			LoadParticles();
		}

		void LoadParticles()
		{
			speedUpParticle = particlePool.GetTowerSpeedUpParticle();
			damageUpParticle = particlePool.GetTowerDamageUpParticle();
		}

		public void PlayDropScaleAnimation()
		{
			dropScaleTween?.Kill();
			dropScaleTween = drop.DOScale(startDropScale * scaleFactor, scaleTweenDuration).SetEase(Ease.OutQuad)
				.SetLoops(2, LoopType.Yoyo);
		}

		public void PlayKnobScaleAnimation()
		{
			knobScaleTween?.Kill();
			knobScaleTween = knob.DOScale(startKnobScale * scaleFactor, scaleTweenDuration).SetEase(Ease.OutQuad)
				.SetLoops(2, LoopType.Yoyo);
		}

		public void PlayDamageUpParticle()
		{
			damageUpParticle.transform.position = transform.position + Vector3.up * 4f;
			damageUpParticle.Play();
		}

		public void PlaySpeedUpParticle()
		{
			speedUpParticle.transform.position = transform.position + Vector3.up;
			speedUpParticle.Play();
		}
	}
}