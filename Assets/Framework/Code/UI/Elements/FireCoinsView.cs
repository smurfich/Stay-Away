﻿using Framework.Code.Infrastructure.Services.PersistentProgress;
using TMPro;
using UnityEngine;
using Zenject;

namespace Framework.Code.UI.Elements
{
	public class FireCoinsView : MonoBehaviour, IViewUpdatable
	{
		[SerializeField] TextMeshProUGUI fireCoinsText;

		IPersistentProgressService progressService;

		[Inject]
		void Construct(IPersistentProgressService progressService)
		{
			this.progressService = progressService;
		}

		void OnEnable()
		{
			progressService.Progress.Collectables.FireAmountChanged += OnFireAmountChanged;
			
			fireCoinsText.text = progressService.Progress.Collectables.FireCoinsAmount.ToString();
		}

		public void UpdateView() => OnFireAmountChanged();
		

		void OnFireAmountChanged() => fireCoinsText.text = progressService.Progress.Collectables.FireCoinsAmount.ToString();

		void OnDisable()
		{
			progressService.Progress.Collectables.FireAmountChanged -= OnFireAmountChanged;

		}
	}
}