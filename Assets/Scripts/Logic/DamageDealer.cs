﻿using Enemies.Visitor;
using UnityEngine;

namespace Logic
{
	public abstract class DamageDealer : MonoBehaviour
	{
		public abstract void Accept(IEnemiesParticlesVisitor particlesVisitor);
	}
}