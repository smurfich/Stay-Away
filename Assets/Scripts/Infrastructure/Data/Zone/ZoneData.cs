﻿using UnityEngine;

namespace Infrastructure.Data.Zone
{
    [CreateAssetMenu(fileName = "Zone Data", menuName = "Design / Zone Data")]
    public class ZoneData : ScriptableObject
    {
        public ZoneType ZoneType;
        public float StartRadius;
        public float EndRadius;
        public float GrowthSpeed;
        public float Strength;
        public float Health;
        public float DeathRadius;
    }
}