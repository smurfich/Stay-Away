﻿using Framework.Code.Data;
using Framework.Code.Factories.Levels;
using Framework.Code.Infrastructure.Services.Analytics;
using Framework.Code.Infrastructure.Services.Assets;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Framework.Code.Infrastructure.Services.SaveSystem;
using Framework.Code.UI;
using Logic.UI;
using UnityEngine.UI;

namespace Framework.Code.Infrastructure.States
{
	public class WinState : IState
	{
		readonly GameStateMachine stateMachine;
		readonly WindowPool windowPool;
		readonly TotalCoinsPassiveView totalCoinsPassiveView;
		readonly IPersistentProgressService progressService;
		readonly ISaveLoadService saveLoadService;
		readonly IAnalyticsService analyticsService;
		readonly ILevelFactory levelFactory;
		readonly GameData gameData;

		Button nextLevelButton;

		public WinState(GameStateMachine stateMachine, WindowPool windowPool,
			TotalCoinsPassiveView totalCoinsPassiveView,
			IPersistentProgressService progressService, ISaveLoadService saveLoadService,
			IAnalyticsService analyticsService, ILevelFactory levelFactory, IAssetProvider assetProvider)
		{
			this.stateMachine = stateMachine;
			this.windowPool = windowPool;
			this.totalCoinsPassiveView = totalCoinsPassiveView;
			this.progressService = progressService;
			this.saveLoadService = saveLoadService;
			this.analyticsService = analyticsService;
			this.levelFactory = levelFactory;

			gameData = assetProvider.Load<GameData>(AssetPath.GAME_DATA);
		}

		public void Enter()
		{
			SubscribeOnButtonClick();
			UpdateUI();
			
			UpdatePlayerProgress();

			analyticsService.LevelCompleted(levelFactory.CurrentLevel.Id, progressService.Progress.Level - 1, true,
				progressService.Progress.Collectables.Amount, levelFactory.CurrentLevel.TimeSpent);
		}

		public void Exit() => nextLevelButton.onClick.RemoveListener(SwitchState);

		void SwitchState()
		{
			stateMachine.Enter<LoadLevelState>();
		}

		void SubscribeOnButtonClick()
		{
			Graphic winPanel = windowPool.GetWindow(WindowType.Win);
			nextLevelButton = winPanel.GetComponentInChildren<Button>();
			if (nextLevelButton != null)
			{
				nextLevelButton.onClick.AddListener(SwitchState);
			}
		}

		void UpdateUI()
		{
			windowPool.DisableWindows(WindowType.LevelCoins);
			windowPool.EnableWindows(WindowType.Win);
			totalCoinsPassiveView.UpdateLevelReward(progressService.Progress.Collectables.LevelAmount);
			totalCoinsPassiveView.UpdateTotalRewardText(progressService.Progress.Collectables.Amount);
		}

		void UpdatePlayerProgress()
		{
			progressService.Progress.Level++;
			progressService.Progress.Collectables.LevelAmount = 0;
			progressService.Progress.Collectables.FireCoinsAmount = 0;

			saveLoadService.Save(progressService.Progress);
		}
	}
}